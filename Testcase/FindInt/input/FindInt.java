/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.algorithmmidterm;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Rattanalak
 */
public class FindInt {

    public static void main(String[] args) {
       Scanner kb = new Scanner(System.in);
        ArrayList<Integer> X = new ArrayList<Integer>();// to find one Integer x
        ArrayList<Integer> N = new ArrayList<Integer>();//to separate twice and one Integer apart.
        int n = kb.nextInt(); // input as number of array .
        int[] A = new int[n];//array to keep input.
         for (int i = 0; i < n; i++) {// loop for to get  input.
            A[i] = kb.nextInt();
        }
        for (Integer a : A) {//find one integer.
            if (!X.contains(a)) {//put One Integer here.
                X.add(a);
            } else {
                N.add(a); //add Integer that already have in X array.
                X.remove(a);// remove Duplicate Integer out from array.

            }
        }
        int x = X.get(0); // get value inside X array.

        System.out.println(x);

    }
}
