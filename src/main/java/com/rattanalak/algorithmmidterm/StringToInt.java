/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.algorithmmidterm;

import java.util.Scanner;

/**
 *
 * @author Rattanalak
 */
public class StringToInt {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String n = kb.nextLine();// number String Format.
        System.out.println(StringToInt(n));

    }

    public static int StringToInt(String n) {
        char[] str = new char[n.length()];
        int result = 0;
        int length = str.length - 1;
        for (int i = 0; i < n.length(); i++) {//Change String to char array. 
            str[i] = n.charAt(i);
        }
        for (int i = 0; i <= str.length - 1; i++) {//Change  char array to int. 
            int index = str[i] - '0';// make integer '0' have vaule  0 not 48.
            result = result * 10;
            result = result + index;
        }
        return result;
    }

}
